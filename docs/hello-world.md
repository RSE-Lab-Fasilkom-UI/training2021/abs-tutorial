# "Hello, World"

Every programming tutorial usually starts with the most basic program. Or, in
the case of ABS, we are going to write a "Hello, World" model. This distinction
is important due to the nature of ABS language as an **executable modeling language**.

## Creating "Hello, World"

First, make sure that you are currently in the location where you cloned the
tutorial repository and create a new text file named `Hello.abs` in `src/main`:

```bash
$ pwd
/home/tutorial/abs-tutorial
$ touch src/main/Hello.abs
$ $EDITOR src/main/Hello.abs
```

In `Hello.abs`, write the following code snippet:

```java
module Hello;

{
    println("Hello, World!");
}
```

Now, compile the ABS module to a Java source code and classfiles:

```bash
$ pwd
/home/tutorial/abs-tutorial
$ java -jar ./bin/absfrontend.jar -j ./src/main
```

You can see the generated Java source code and class files in `gen`
directory. To run the program:

```bash
$ pwd
/home/tutorial/abs-tutorial
$ java -cp "bin/absfrontend.jar:gen" Hello.Main
Hello, World!
```

> Note: For Windows users, replace `:` (colon) with `;` (semicolon) in the
> classpath value in the example.

## Creating "Hello, World" in Eclipse

You will create a "Hello, World" model in ABS language using Eclipse and ABS
plugin. Create a new ABS project and give the new project a name, e.g.,
_Tutorial_. The new project will appear in the _Project Explorer_.

Right-click the new project and choose to create a new ABS module named
`Hello.abs`. A text editor will open the empty module file and appear in
Eclipse. In the text editor, write the following:

```java
module Hello;

{
    println("Hello, World!");
}
```

Run the model by right-click empty space on the text editor then choose
_Run As_ -> _ABS Java Backend_ or press CTRL-F11 on keyboard. The output
will appear in the _Console_ panel.

![Example of "Hello, World!" program output](images/hello-world-output.png)

## Explanation

Remember that ABS is a **modeling** language, or more appropriately, an
**executable modeling** language. As its name suggests, ABS is mainly used for
modeling the behavior of components in a system, including the interactions
between one or more components. Since component interaction may include state
changes, imperative procedures, and communication among components, ABS can
capture those elements as text-based model.

ABS does not provide any compiler or runtime environment that is able to run
the model. But instead, ABS has several backends that is able to transform the
model into an actual programming language such as Java. Once the model
transformed to a programming language, we can use the existing toolchain on the
target programming language to compile and run the model as program.

For example, when you run the "Hello, World" model, the ABS plugin actually
transformed the ABS model to a Java code, compile the Java code to an
executable Java program, and run the compiled Java program. You can see the
transformation result in `gen` directory under the _Tutorial_ project.

![Side-by-side comparison of ABS and generated Java code](images/hello-world-generated-java.png)

## Next Instruction

We have seen how to create a simple "Hello, World" in ABS. Now, we are going
to explore ABS language features by developing a model of bank application
in ABS.
